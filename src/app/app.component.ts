import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `<h1 class="title">Hello {{name}}</h1> 
             <br> 
             <h1 class="title"></h1>
             <h1 id="hora" class="title">Hora: {{tiempo}}</h1>
             <br>
             <h1 class="title">Tu navegador es: {{ver}}</h1>
             <br>
             <h1 class="title">Tu dispositivo es: {{mach}}</h1>
             <br>
             <h1 class="title">Tu sistema operativo es: {{so}}</h1>`,
   styles: [`
    .title {
      color: #1E90FF;
      font-size: 20px;
    }`]
})

export class AppComponent{ 
  name = 'Antonio :)'; 
  tiempo: Date;
  chrome: Boolean;
  firefox: Boolean;
  explorer: Boolean;
  ver: String;
  win: Boolean;
  linux: Boolean;
  mac: Boolean;
  so: String;
  disp: Boolean;
  mach: String;

  constructor(){

      this.chrome = (navigator.userAgent.indexOf("Chrome") != -1);
      if (this.chrome){
        this.ver = "Chrome";
        setInterval(() => this.tiempo = new Date(), 1000);
      }
      
      this.firefox = (navigator.userAgent.indexOf("Firefox") != -1);
      if (this.firefox){
        this.ver = "FireFox";
        setInterval(() => this.tiempo = new Date(), 1000);
      }
      
      this.explorer = (navigator.userAgent.indexOf("Edge") != -1);
      if (this.explorer){
        this.ver = "Edge";
        alert("Estás en Edge, cámbiate a otro navegador. Piojo!");
      }
      

      this.win = (window.navigator.appVersion.indexOf("Win") != -1);
      if(this.win){
        this.so = "Windows";
      }

      this.linux = (window.navigator.appVersion.indexOf("Linux") != -1);
      if(this.linux){
        this.so = "Linux";
      }

      this.mac = (window.navigator.appVersion.indexOf("Mac") != -1);
      if(this.mac){
        this.so = "Macintosh";
      }

      this.disp = (navigator.platform.indexOf("Win32") != -1);
      if(this.disp)
        this.mach = "Escritorio o Laptop";
      else
        this.mach = "Móvil";
    }


}